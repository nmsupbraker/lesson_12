import warnings

import requests

warnings.filterwarnings("ignore")

response = requests.get("http://localhost:3000")
assert response.status_code == 200
assert response.text.lower().index("hello") > 0
